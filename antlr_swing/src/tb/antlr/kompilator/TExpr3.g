tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  Integer blockNumber = 0;
}
//prog    : (e+=expr | d+=decl)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";
prog    : (e+=expr | d+=decl | e+=block)* -> template(name={$e},deklaracje={$d}) "<deklaracje> start: <name;separator=\" \n\"> ";

block   : ^(LB {blockNumber++; ls.enterScope();} (e+=block | e+=expr | d+=decl)* {blockNumber--; ls.leaveScope();}) -> block(name={$e}, deklaracje={$d});


decl  :
        //^(VAR i1=ID) {globals.newSymbol($ID.text);} -> dek(n={$ID.text})
        ^(VAR i1=ID) {ls.newSymbol($ID.text);} -> dek(n={$ID.text}, varID={numer}, blockID={blockNumber.toString()})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> add(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> subtract(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> multiply(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> divide(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {ls.getSymbol($i1.text);} -> setLocalVar(p1={$i1.text},p2={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | i1=ID                       {ls.getSymbol($i1.text);} -> getLocalVar(p1={$i1.text})
    ;
    