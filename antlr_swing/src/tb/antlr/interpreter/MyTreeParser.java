package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols gs = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(int x, int y) {
		return x+y;
	}
	
	protected Integer subtract(int x, int y) {
		return x-y;
	}
	
	protected Integer multiply(int x, int y) {
		return x*y;
	}
	
	protected Integer divide(int x, int y) throws ArithmeticException {
		if(x == 0) throw new ArithmeticException ("Division by 0"); 
		return x/y;
	}
	
	protected Integer modulo(int x, int y) {
		return x%y;
	}
	
	protected Integer power(int base, int index) {
		if(index == 0) return 1;
		int result = 1;
		for(int i=0; i<index; i++) {
			result = result * base;
		}
		return result;
	}
	
	protected void declareGlobalVar(String var) {
		gs.newSymbol(var);
	}
	
	protected void setGlobalVar(String var, int value) {
		gs.setSymbol(var, value);
	}
	
	protected Integer getGlobalVar(String var) {
		return gs.getSymbol(var);
	}
	
	
}
