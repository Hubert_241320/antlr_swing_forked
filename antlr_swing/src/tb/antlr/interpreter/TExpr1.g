tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

//prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;
prog    : (expr | decl)* ;

decl    : ^(VAR i1=ID) {declareGlobalVar($i1.text);};

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);} /*{$out = $e1.out + $e2.out;}*/
        | ^(MINUS e1=expr e2=expr) {$out = subtract($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(POW   e1=expr e2=expr) {$out = power($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = modulo($e1.out, $e2.out);}
        | ^(PODST i1=ID   e2=expr) {$out = $e2.out; setGlobalVar($i1.text, $e2.out);}
        | ID                       {$out = getGlobalVar($ID.text);}
        | INT                      {$out = getInt($INT.text);}
        | ^(PRINT e=expr)          {$out = $e.out; drukuj($e.out.toString());}
        ;
